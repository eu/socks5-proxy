# S5S: a simple SOCKS5 proxy server

A SOCKS5 proxy server creates a Transmission Control Protocol (TCP) connection to another server behind the firewall on the client's behalf, then exchanges network packets between the client and the actual server.
### Features
- Runs as a windows service
- Uses port 1080
- Multithreading (specified in s5s.h)
- TCP only without authentication
### Build
```sh
mingw32-make
```

### Usage
```sh
s5s --install
s5s --remove
```
After installation, you need to restart your computer or manually start "s5s" the service using system utilities
### License: ISC

