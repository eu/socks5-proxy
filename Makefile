CC = gcc
CFLAGS = -O1 -Wall -Wextra -mthreads
LDFLAGS =
LIBS = -mwindows -lws2_32

all: s5s

s5s:	main.o service.o net.o thread.o poll.o
	$(CC) $(LDFLAGS) $^ $(LIBS) -o $@.exe
#	$(CC) $(LDFLAGS) $(LIBS) $^ -o $@

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@cls
	@del *.o s5s.exe
