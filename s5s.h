/*
 * Copyright (c) 2010 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define SERVICE_NAME	"s5s"
#define SERVER_PORT	1080
#define TIMEOUT_RECV	200
#define TIMEOUT_SEND	2000
#define MAX_THREADS	64
#define RCV		1024
#define POLLIN		1
#define POLLOUT		2
#define ASYNC_MSG_SRV	(WM_USER + 1)

struct pollfd {
	SOCKET	fd;
	short	events;
	short	revents;
};

int  service_install(void);
int  service_remove(void);
void WINAPI service_main(void);
int  net_startup(void);
void net_cleanup(void);
int  net_start_server(void);
void net_stop_server(void);
void net_cycle(void);
int  poll(struct pollfd *, int);
DWORD WINAPI thread_proc(void *);
