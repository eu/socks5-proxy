/*
 * Copyright (c) 2010 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <winsock2.h>
#include <windows.h>
#include "s5s.h"

extern char service_name[];

int WINAPI WinMain(HINSTANCE this_inst __attribute__((unused)),
		   HINSTANCE prev_inst __attribute__((unused)), LPSTR command_line,
		   int state __attribute__((unused)))
{
	if (!lstrcmp(command_line, "--install")) {
		if (!service_install()) {
			MessageBox(0, "Could not install service.", SERVICE_NAME, MB_OK);
			return 0;
		}
	}

	if (!lstrcmp(command_line, "--remove")) {
		if (!service_remove()) {
			MessageBox(0, "Could not remove service.", SERVICE_NAME, MB_OK);
			return 0;
		}
	}

	SERVICE_TABLE_ENTRY ste[] = {
		{service_name, (LPSERVICE_MAIN_FUNCTION) service_main},
		{NULL, NULL}
	};

	if (!StartServiceCtrlDispatcher(ste)) { }

	return 0;
}
