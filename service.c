/*
 * Copyright (c) 2010 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <winsock2.h>
#include <windows.h>
#include "s5s.h"

void WINAPI service_control(DWORD);

SERVICE_STATUS_HANDLE hss;
char service_name[] = SERVICE_NAME;

static int set_service_status(DWORD state, DWORD exit_code, DWORD progress)
{
	SERVICE_STATUS ss;

	ss.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	ss.dwCurrentState = state;
	ss.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
	ss.dwWin32ExitCode = exit_code;
	ss.dwServiceSpecificExitCode = 0;
	ss.dwCheckPoint = progress;
	ss.dwWaitHint = 3000;
	return SetServiceStatus(hss, &ss);
}

static int service_start()
{
	set_service_status(SERVICE_RUNNING, 0, 0);

	if (!net_startup() || !net_start_server())
        	return 0;

	net_cycle();

	return 1;
}

static int service_stop()
{
	net_stop_server();
        net_cleanup();
	return 0;
}

int service_install()
{
	CHAR sdir[MAX_PATH];

	SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if (scm == NULL)
		return 0;

	GetCurrentDirectory(MAX_PATH, sdir);
	lstrcat(sdir, "\\");
	lstrcat(sdir, SERVICE_NAME);
	lstrcat(sdir, ".exe");

	SC_HANDLE svc;
	svc = CreateService(scm, SERVICE_NAME, SERVICE_NAME, 0,
			SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
			SERVICE_AUTO_START, SERVICE_ERROR_IGNORE,
			sdir, NULL, NULL, NULL, NULL, NULL);

	CloseServiceHandle(scm);
	if (svc == NULL)
		return 0;

	CloseServiceHandle(svc);

	MessageBox(0, "Service succesful created.", SERVICE_NAME, MB_OK);
	return 1;
}


int service_remove()
{
	SC_HANDLE scm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (scm == NULL)
		return 0;

	SC_HANDLE svc = OpenService(scm, SERVICE_NAME, STANDARD_RIGHTS_REQUIRED);
	DeleteService(svc);

	CloseServiceHandle(scm);
	if (svc == NULL)
		return 0;

	CloseServiceHandle(svc);
	MessageBox(0, "Service removed.", SERVICE_NAME, MB_OK);
	return 1;
}

void WINAPI service_main()
{
	hss = RegisterServiceCtrlHandler(service_name, (LPHANDLER_FUNCTION) service_control);
	if (!hss)
		return;

	set_service_status(SERVICE_START_PENDING, 0, 1);
	if (!service_start())
		service_stop();

	return;
}

void WINAPI service_control(DWORD command)
{
	switch(command) {
		case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN:
			set_service_status(SERVICE_STOP_PENDING, 0, 1);
			service_stop();
			set_service_status(SERVICE_STOPPED, 0, 0);
			break;
		default:
			break;
	}
}

