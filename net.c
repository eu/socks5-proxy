/*
 * Copyright (c) 2010 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <winsock2.h>
#include "s5s.h"

extern CRITICAL_SECTION hum;
extern int threads;

SOCKET server_socket;
WSAEVENT he;

int net_startup()
{
	WSADATA wsaData;
	WORD version_requested = MAKEWORD(2, 0);
	int err = WSAStartup(version_requested, &wsaData);
	if (err)
		return 0;

	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 0) {
		WSACleanup();
		return 0;
	}

	InitializeCriticalSection(&hum);
	threads = 0;
        return 1;
}

void net_cleanup()
{
	WSACleanup();
}

int net_start_server()
{
	SOCKADDR_IN srv_sa;

	server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket == INVALID_SOCKET)
		return 0;

	he = WSACreateEvent();
	if (he == WSA_INVALID_EVENT)
		return 0;

	int err = WSAEventSelect(server_socket, he, FD_ACCEPT);
	if (err == SOCKET_ERROR) {
		closesocket(server_socket);
		return 0;
	}

	srv_sa.sin_family = AF_INET;
	srv_sa.sin_addr.s_addr = INADDR_ANY;
        srv_sa.sin_port = htons(SERVER_PORT);
        err = bind(server_socket, (struct sockaddr *) &srv_sa, sizeof(srv_sa));
        if (err == SOCKET_ERROR) {
		closesocket(server_socket);
                return 0;
        }

	err = listen(server_socket, MAX_THREADS);
	if (err == SOCKET_ERROR) {
		closesocket(server_socket);
		return 0;
	}

	return 1;
}


void net_stop_server()
{
	shutdown(server_socket, SD_BOTH);
	closesocket(server_socket);
}

SOCKET accept_client()
{
	SOCKADDR_IN sa;

	int len = sizeof(SOCKADDR_IN);
	SOCKET sock = accept(server_socket, (struct sockaddr *) &sa, &len);
	if (sock == INVALID_SOCKET)
		return sock;

	unsigned long mode = 1;
	ioctlsocket(sock, FIONBIO, &mode);

	return (SOCKET) sock;
}

void net_cycle()
{
	while (1) {
		WSANETWORKEVENTS net_events;
		SOCKET sock;
		DWORD thread_id;

		DWORD event = WSAWaitForMultipleEvents(1, &he, FALSE, INFINITE, FALSE);
		if (event == WSA_WAIT_FAILED)
			return;

		WSAEnumNetworkEvents(server_socket, he, &net_events);

		if (net_events.lNetworkEvents & FD_ACCEPT) {
         		if (net_events.iErrorCode[FD_ACCEPT_BIT])
				return;

			/* create thread */
			sock = accept_client();
			if (sock == INVALID_SOCKET)
				return;

			CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) thread_proc,
				     &sock, 0, &thread_id);
		}
	}
}

