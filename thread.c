/*
 * Copyright (c) 2010 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <winsock2.h>
#include "s5s.h"

CRITICAL_SECTION hum;
int threads;

static int socksend(SOCKET sock, char *buf, int bufsize)
{
	int err, ns, sent;
	struct pollfd fds;

	fds.fd = sock;
	fds.events = POLLOUT;

	sent = 0;
	do {
		err = poll(&fds, TIMEOUT_SEND);
		if (err < 0 && errno == WSAEWOULDBLOCK)
			continue;

		if (err < 1)
			break;

		ns = send(sock, buf + sent, bufsize - sent, 0);

		if (ns < 0 && errno == WSAEWOULDBLOCK)
			continue;

		if (!ns || ns < 0)
			break;

		sent += ns;
	} while (sent < bufsize);

	return sent;
}

static int sockrecv(SOCKET sock, char *buf, int bufsize)
{
	int err, nr;
	struct pollfd fds;

	fds.fd = sock;
	fds.events = POLLIN;

	err = poll(&fds, TIMEOUT_RECV);
	if (err < 1)
		return err;
	do
		nr = recv(sock, buf, bufsize, 0);
	while (nr < 0 && errno == WSAEWOULDBLOCK);

	return nr;
}

static int auth(SOCKET sock)
{
	char buf[20];

	/* recv_auth_method */
	int n = sockrecv(sock, buf, 3);
	if ((n != 3) || (buf[0] != 5))
		return 0;

	/* send_auth_method */
	buf[1] = 0;
	n = socksend(sock, buf, 2);

	return (n == 2);
}

static SOCKET make_connect(SOCKET client_sock)
{
	int err, n;
	SOCKADDR_IN sa;
	SOCKET sock = INVALID_SOCKET;
	char buf[256];

	n = sockrecv(client_sock, buf, 256);
	/* 5 1 0 1 ip   port */
	if (n > 9 && buf[0] == 5 && buf[1] == 1 && !buf[2]) {
		unsigned short port;

		if (buf[3] == 1) {
			// IP
			memcpy(&sa.sin_addr.s_addr, buf + 4, 4 * sizeof(char));
			port = (unsigned char) buf[8] * 256 + (unsigned char) buf[9];
		} else if (buf[3] == 3 && n >= 7 + (unsigned char) buf[4]) {
			// DNS
			char name[256];
			unsigned char i = (unsigned char) buf[4];
			memcpy(name, buf+4+1, i);
			name[i] = 0;

			LPHOSTENT host_info = gethostbyname(name);
			if (host_info == NULL)
				return sock;

			port = (unsigned char) buf[5+i] * 256 + (unsigned char) buf[6+i];

			i = 0;
			if (host_info->h_addrtype == AF_INET && host_info->h_addr_list[0])
				sa.sin_addr.s_addr = *(unsigned long *) host_info->h_addr_list[0];
			else
				return sock; //NETBIOS address

		} else
			return sock;

		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock == INVALID_SOCKET) {
			socksend(client_sock, buf, n);
			return sock;
		}

		sa.sin_family = AF_INET;
		sa.sin_port = htons(port);

		err = connect(sock, (struct sockaddr *) &sa, sizeof(sa));
		if (err == SOCKET_ERROR) {
			closesocket(sock);
			socksend(client_sock, buf, n);
			return INVALID_SOCKET;
		}
/*
 * Check connection
 *
		char msg[512];
		sprintf(msg, "ip: %s:%d", inet_ntoa(sa.sin_addr), port);
		MessageBox(0, msg, SERVICE_NAME, MB_OK);
*/
		unsigned long mode = 1;
		ioctlsocket(sock, FIONBIO, &mode);

		buf[1] = 0;
		socksend(client_sock, buf, n);
	}

	return (SOCKET) sock;
}

static void resend(SOCKET is, SOCKET os)
{
	int nr, ns;
        char buf[RCV];

	while (1) {
		nr = sockrecv(is, buf, RCV);
		if (nr < 1)
			break;

		ns = socksend(os, buf, nr);
		if (ns != nr)
			break;
	}
}

DWORD WINAPI thread_proc(void *p)
{
	SOCKET cs = *(SOCKET *)p;
	SOCKET ds;

	EnterCriticalSection(&hum);
	threads++;
	LeaveCriticalSection(&hum);

	if (auth(cs)) {
		ds = make_connect(cs);
		if (ds != INVALID_SOCKET) {
			resend(cs, ds);
			resend(ds, cs);
			closesocket(ds);
		}
	}
	closesocket(cs);

	EnterCriticalSection(&hum);
	threads--;
	LeaveCriticalSection(&hum);

	return 0;
}

