/*
 * Copyright (c) 2010 Eugene P. <pieu@mail.ru>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <winsock2.h>
#include "s5s.h"

int poll(struct pollfd *fds, int timeout)
{
	struct timeval tv;
	tv.tv_sec = timeout / 1000;
	tv.tv_usec = (timeout % 1000) * 1000;

	fd_set readfds;
	fd_set writefds;
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);

	if ((fds->events & POLLIN))
		FD_SET(fds->fd, &readfds);

	if ((fds->events & POLLOUT))
		FD_SET(fds->fd, &writefds);

	fds->revents = 0;

	SOCKET maxfds = 0;
	if (fds->fd > maxfds)
		maxfds = fds->fd;

	int err = select(((int)(maxfds))+1, &readfds, &writefds, NULL, &tv);
	if (err < 1)
		return err;

	if (FD_ISSET(fds->fd, &readfds))
		fds->revents |= POLLIN;

	if (FD_ISSET(fds->fd, &writefds))
		fds->revents |= POLLOUT;

	return err;
}
